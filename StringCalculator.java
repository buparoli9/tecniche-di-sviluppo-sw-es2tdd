package Es2;

import java.util.stream.Stream;

public class StringCalculator {

    static private int limite_numero = 1000;
    static private String limitatore_default = ",";

    static public int add(String numbers) throws NumberFormatException{
        //controllo di stringa vuota
        if(numbers.isEmpty()){
            return 0;
        }
        
        //Converto i delimitatori passati alla virgola
        if(numbers.startsWith("//")){
            numbers = StringCalculator.conversioneDelimitatori(numbers);
        }
            
        //Rimozione di newlines
        numbers = numbers.replace("\n", "");

        //split della stringa
        String[] splitted_numbers = numbers.split(StringCalculator.limitatore_default);

        //conversione in interi
        Integer[] numbers_converted = Stream.of(splitted_numbers).map(Integer::valueOf).toArray(Integer[]::new);
        
        //Controllo di negativi e raccolta del loro elenco
        String messaggio_errore = StringCalculator.controlloNegativi(numbers_converted);

        if(!messaggio_errore.isEmpty()){
            throw new NumberFormatException("Negatives not allowed: " + messaggio_errore);
        }

        int sum = 0;
        for (Integer number : numbers_converted) {
            //Non vuole roba maggiore di 1000
            if(number <= StringCalculator.limite_numero){
                sum += number;
            }  
        }

        return sum;
    }

    /**Metodo per estrazione del delimitatore se specificato */
    static private String conversioneDelimitatori(String numbers){
        //Se è una stringa di cui si specificano i separator gli estraggo li converto alla ,
        String delimiter = "";
        String[] delimiters;

        //estraggo caratteri a partire dal primo utile
        int i = 3;
        while(numbers.charAt(i) != '\n'){
            delimiter = delimiter + numbers.charAt(i);
            i++;
        }
        //Rimuovo da numbers la parte dei delimitatori
        numbers = numbers.substring(i+1);
        //Ora ho i segli separati da parentesi quadre, rimuovo prima e ultima e uso ][ come separatore
        delimiter = delimiter.substring(0, delimiter.length()-1);
        delimiter = delimiter.replace("][",StringCalculator.limitatore_default);
        delimiters = delimiter.split(StringCalculator.limitatore_default);

        //sostituisco nella stringa i delimitatori con una virgola
        for (String delimitatore : delimiters) {
            numbers = numbers.replace(delimitatore, StringCalculator.limitatore_default);
        }

        return numbers;
    }

    static private String controlloNegativi(Integer[] numeri){
        String elenco_negativi = "";
        for (Integer numero : numeri) {
            if(numero<0){
                if(elenco_negativi.isEmpty()){
                    elenco_negativi += numero;
                }else{
                    elenco_negativi += "," + numero;
                }
            }
        }

        return elenco_negativi;
    }
}
