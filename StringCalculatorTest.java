package Es2;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.Assertions;

public class StringCalulatorTest {

    @Test
    public void testAddBase(){
        Assert.assertEquals(
            StringCalculator.add(""), 
            0
            );
            Assert.assertEquals(
            StringCalculator.add("1"), 
            1
            );
            Assert.assertEquals(
            StringCalculator.add("1,2"), 
            3
            );
            Assert.assertEquals(
            StringCalculator.add("1,2,6,8"), 
            17
            );
    }

    @Test
    public void testAddWithNewline(){
        Assert.assertEquals(
            StringCalculator.add("1\n,2,3"),
            6
        );
    }

    @Test
    public void testDelimiter(){
        Assert.assertEquals(
            StringCalculator.add("//[;]\n1;2;3\n;4"),
            10
        );
    }

    @Test
    public void testNegative(){
        Exception exception = assertThrows(
            NumberFormatException.class, () -> {
            StringCalculator.add("1,2,3,-4,-5,-7,8");
        });

        String expectedMessage = "Negatives not allowed: -4,-5,-7";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testSumGrater1000(){
        Assert.assertEquals(
            StringCalculator.add("2,1001"),
            2
            );
    }

    @Test
    public void testDelimiterAnyLenght(){
        Assert.assertEquals(
            StringCalculator.add("//[***]\n1***2***3\n***4"),
            10
        );
    }

    @Test
    public void testMultipleDelimiters(){
        Assert.assertEquals(
            StringCalculator.add("//[*][%]\n1*2\n%3"),
            6
        );
    }

    @Test
    public void testLongerMultipleDelimiters(){
        Assert.assertEquals(
            StringCalculator.add("//[***][%%][;]\n1***2\n%%3;5"),
            11
        );
    }
}
